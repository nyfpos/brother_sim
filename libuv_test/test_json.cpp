#include "stdafx.h"
#include "test_json.h"
#include <nlohmann/json.hpp>

using json = nlohmann::json;

namespace test_json
{
	void test1()
	{
		// create an array using push_back
		json j;
		j.push_back("foo");
		j.push_back(1);
		j.push_back(true);

		// also use emplace_back
		j.emplace_back(1.78);

		// iterate the array
		for (json::iterator it = j.begin(); it != j.end(); ++it) {
			std::cout << *it << '\n';
		}

		// range-based for
		for (auto& element : j) {
			std::cout << element << '\n';
		}

		// getter/setter
		const std::string tmp = j[0];
		j[1] = 42;
		bool foo = j.at(2);

		// comparison
		j == "[\"foo\", 1, true]"_json;  // true

										 // other stuff
		j.size();     // 3 entries
		j.empty();    // false
		j.type();     // json::value_t::array
		j.clear();    // the array is empty again

					  // convenience type checkers
		j.is_null();
		j.is_boolean();
		j.is_number();
		j.is_object();
		j.is_array();
		j.is_string();

		// create an object
		json o;
		o["foo"] = 23;
		o["bar"] = false;
		o["baz"] = 3.141;

		// also use emplace
		o.emplace("weather", "sunny");

		// special iterator member functions for objects
		for (json::iterator it = o.begin(); it != o.end(); ++it) {
			std::cout << it.key() << " : " << it.value() << "\n";
		}

		// find an entry
		if (o.find("foo") != o.end()) {
			// there is an entry with key "foo"
		}

		// or simpler using count()
		int foo_present = o.count("foo"); // 1
		int fob_present = o.count("fob"); // 0

										  // delete an entry
		o.erase("foo");
	}

	void test2()
	{
		auto o = json::parse("{ \"happy\": true, \"pi\": 3.141 }");
		std::cout << "o['happy']=" << o["happy"] << " "
			<< "o['pi']=" << o["pi"] << std::endl;
	}
}