#pragma once
#include "http_api_controller.h"
#include "webserver.h"
namespace waweb
{
	class http_request_handler
	{
	public:
		http_request_handler(iwa_webapi srvApi);
		~http_request_handler();

		void get_resp(struct message&, char**, int*);
	private:
		http_api_controller _api_contrl;
	};

}