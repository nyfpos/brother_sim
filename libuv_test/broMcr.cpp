#include "stdafx.h"
#include "broMcr.h"
#include "util.h"

namespace wa
{
	broMcr::broMcr()
	{
	}

	broMcr::~broMcr()
	{
	}

	bool broMcr::set_val(std::string key, std::string val)
	{
		bool ok = false;
		if (_val.find(key) != _val.end())
		{
			_val[key] = val;
			ok = true;
		} 
		return ok;
	}

	void broMcr::set(std::string src)
	{
		std::istringstream iss(src);
		std::string line;
		while (std::getline(iss, line))
		{
			if (line[0] == 'C' || line[0] == 'c')
			{
				std::vector<std::string> v = util::split(line, ",");
				if (v.size() > 0)
				{
					std::string key = std::move(v[0]);
					if (v.size() > 1)
						_val[key] = v[1];
					else
						_val[key] = "";
				}
			}
		}
	}

	void broMcr::get(std::string& out)
	{
		std::string key;
		char tmp[6] = { 0 };
		std::string ot("%RLOD    MCRNM1  00\r\n");
		for (int i = 500; i < 1000; i++)
		{
			sprintf_s(tmp, "C%3d", i);
			key = tmp;
			if (_val.find(key) != _val.end())
			{
				ot.append(key + "," + _val[key] + "\r\n");
			}
			else {
				ot.append(key + ",\r\n");
			}
		}
		ot.append("\r\n");
		ot.append("10%");
		out = ot;
	}
}