#include "stdafx.h"
#include "test_net.h"

#define MAX_CONNECT 1024
namespace test_net_cli
{
#define log(x) printf("%s\n", x);
	char* message;
	uv_buf_t _wr_buf[1] = { 0 };
	int nummsg = 0;

	void on_connect(uv_connect_t *req, int status);
	void on_write_end(uv_write_t *req, int status);
	void alloc_buffer(uv_handle_t *handle, size_t suggested_size, uv_buf_t* buf);
	void echo_read(uv_stream_t *server, ssize_t nread, const uv_buf_t* buf);


	void echo_read(uv_stream_t *server, ssize_t nread, const uv_buf_t* buf) {
		if (nread < 0) {
			if (nread != UV_EOF) {
				printf("Read error %s! closed\n", uv_err_name(nread));
			}
			else {
				printf("Read error EOF! closed\n");
			}
			uv_close((uv_handle_t*)server, NULL);			
		}
		else if(nread > 0) {
			char* tmp = (char*)malloc(nread);
			memset(tmp, 0, nread);
			memcpy_s(tmp, nread, buf->base, nread);
			printf("%s", tmp);
			free(tmp);
			free(buf->base);
			nummsg -= nread;
			if (nummsg <= 0) {
				printf("Read done! closed\n");
				uv_close((uv_handle_t*)server, NULL);
			}
		} else {
			printf("Read done! closed\n");
			uv_close((uv_handle_t*)server, NULL);
		}
	}
	
	void alloc_buffer(uv_handle_t *handle, size_t suggested_size, uv_buf_t* buf) {
		*buf = uv_buf_init((char*)malloc(suggested_size), suggested_size);
	}

	void on_write_end(uv_write_t *req, int status) {
		if (status < 0) {
			if (status == UV_EAGAIN)
			{
				printf("UV_EAGAIN");
				return;
			}
			else {
				printf("error on_write_end %d", status);
				return;
			}
		}	
		uv_read_start(req->handle, alloc_buffer, echo_read);
		free(req);
	}
	
	void on_connect(uv_connect_t *req, int status) {
		if (status == -1) {
			printf("error on_write_end\n");
			return;
		}
				
		int len = *((int*)req->data);
		char* buf = (char*)malloc(len);
		memcpy_s(buf, len, message, len);		
		buf[len - 1] = 0;
		_wr_buf[0] = uv_buf_init(buf, len);
		_wr_buf[0].len = len;
		_wr_buf[0].base = buf;
		nummsg = len;

		uv_stream_t* tcp = req->handle;
		uv_write((uv_write_t*)malloc(sizeof(uv_write_t)), tcp, _wr_buf, 1, on_write_end);
	}
	void start_tcp_client(const char* ip, int port, const char* msg, size_t n)
	{		
		message = (char*)malloc(n);		
		if (message)
		{
			memset(message, 0, n);			
			memcpy_s(message, n, msg, n);			
		}
		else {
			printf("message? not alloc\n");
			return;
		}

		uv_loop_t*loop = uv_default_loop();

		uv_tcp_t socket;
		uv_tcp_init(loop, &socket);
		uv_tcp_keepalive(&socket, 1, 60);

		//struct sockaddr_in dest = uv_ip4_addr("0.0.0.0", 1234);
		struct sockaddr_in dest;
		uv_ip4_addr(ip, port, &dest);

		int* d = (int*)malloc(sizeof(int));
		*d = n;
		uv_connect_t connect;
		connect.data = d;
		uv_tcp_connect(&connect, &socket, (const struct sockaddr*)&dest, on_connect);

		uv_run(loop, UV_RUN_DEFAULT);

		free(d);
	}
}