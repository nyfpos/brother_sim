#pragma once

/*
So to clarify the current behaviour:

Listening on :: results in v4 and v6 listening (dual-stack), ss displaying only the v6-style listener.
Listening on 0.0.0.0 results in just v4 listening, ss correctly shows just the v4-style listener.
This dual-stack behaviour looks to only apply to Linux. On OS X and Windows, I can bind :: and 0.0.0.0 simultaneously.

Would it be feasible to disable dual-stack mode if an explicit IPv6 style address is given to .listen?
*/

namespace test_net
{
	struct req_uv_stream
	{
		uv_tcp_t  server;
		uv_loop_t* loop;
	};

	struct req_uv_work
	{
		uv_work_t work;
		uv_loop_t* default_loop;
		char ip[64];
		int port;
	};


	void start_tcp_server(const char* ip, int port);	
}

namespace test_net_cli
{
	void start_tcp_client(const char* ip, int port, const char* msg, size_t n);
}
