#include "stdafx.h"
#include "test_event.h"

namespace test_event
{

	/*
	typedef struct
	{
	uv_idle_t handle;
	int64_t counter;
	} _idle_req;
	void idle_cb(uv_idle_t* handle)
	{
	_idle_req* req = (_idle_req*)handle;
	printf("Idle callback %d\n", req->counter);
	req->counter++;
	if (req->counter >= 5)
	{
	uv_stop(uv_default_loop());
	printf("uv_stop() called\n");
	}
	}
	void prep_cb(uv_prepare_t* handle)
	{
	printf("Prep callback\n");
	}
	*/

	typedef struct
	{
		uv_fs_event_t handle;
		int data[10];
	} _fs_event_req;

	void on_changed(uv_fs_event_t *handle, const char *filename, int events, int status)
	{
		_fs_event_req* req = (_fs_event_req*)handle;
		char path[1024] = { 0 };
		size_t size = 1023;
		uv_fs_event_getpath(handle, path, &size);
		path[size] = 0;

		printf("Change detected in %s: ", path);
		if (events & UV_RENAME)
			printf("renamed");
		if (events & UV_CHANGE)
			printf("changed");

		printf(" %s\n", filename ? filename : "");
	}

	void start()
	{
		/*
		_idle_req* idler = NULL;
		uv_prepare_t prep;
		idler = (_idle_req*)malloc(sizeof(_idle_req));
		memset(0, idler, sizeof(_idle_req));
		if (idler)
		{
		uv_idle_init(uv_default_loop(), &(idler->handle));
		uv_idle_start(&(idler->handle), idle_cb);

		uv_prepare_init(uv_default_loop(), &prep);
		uv_prepare_start(&prep, prep_cb);

		uv_run(uv_default_loop(), UV_RUN_DEFAULT);
		}
		*/
		char* filename = "c:/test1.txt";
		uv_loop_t* loop = uv_default_loop();
		printf("Adding watch on %s\n", filename);

		_fs_event_req* req = (_fs_event_req*)malloc(sizeof(_fs_event_req));
		memset(req, 0, sizeof(_fs_event_req));
		uv_fs_event_init(loop, &(req->handle));
		uv_fs_event_start(&(req->handle), on_changed, filename, UV_FS_EVENT_RECURSIVE);

		uv_run(loop, UV_RUN_DEFAULT);
	}
}
