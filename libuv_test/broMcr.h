#pragma once
#include <map>

namespace wa
{
	class broMcr
	{
	public:
		broMcr();
		~broMcr();

		void set(std::string src);
		void get(std::string& out);
		bool set_val(std::string key, std::string val);
	private:
		std::map<std::string, std::string> _val;
	};

}
