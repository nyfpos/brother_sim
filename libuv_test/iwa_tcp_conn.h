#pragma once

namespace wa
{
	enum tcp_conn_stat
	{
		Unknown = -1,
		Connected = 0,
		Disconnected = 1
	};

	class iwa_tcp_conn
	{
	public:
		iwa_tcp_conn():_connStat(Unknown) { }
		virtual ~iwa_tcp_conn() = 0 { }
		virtual void on_read_some(const char* buf, int nread) = 0;
		virtual bool be_read_done() = 0;
		virtual void get_write_buf(char** buf, int* size) = 0;
		virtual void set_conn_stat(tcp_conn_stat connStat)
		{
			_connStat = connStat;
		}
		virtual tcp_conn_stat get_conn_stat()
		{
			return _connStat;
		}
	private:
		tcp_conn_stat _connStat;
	};
}