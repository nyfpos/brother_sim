#pragma once
#include "stdafx.h"

#include <iostream>
#include <fstream>


#include "iwa_tcp_conn.h"
#include "wastream.h"
#include "util.h"
#include "lock.h"
#include "broMcr.h"

namespace wa
{
	struct broPoData
	{
		std::map<std::string, std::string> ackdict;		
	};

	class broConn : iwa_tcp_conn
	{
	public:
		static int DeviceType;

		broConn(int port);
		virtual ~broConn();

		virtual void on_read_some(const char* buf, int nread)
		{
			_stream.capacity(nread);
			_stream.write((Byte*)buf, nread);
		}

		virtual bool be_read_done()
		{
			char* p = (char*)_stream.begin();
			int sign = 0, sz = _stream.size();
			while (sz > 0 && p)
			{
				if (*p == '%')				
					++sign;				
				--sz;
				++p;
			}
			return sign >= 2;
		}

		virtual void get_write_buf(char** buf, int* size)
		{
			broPoData* podata = NULL;
			{
				THREAD_LIB_LOCK_CRITICAL_SESSION(_poLock)
				if (_poData.find(_port) == _poData.end())
					_poData[_port] = new broPoData();
				podata = _poData[_port];			
				std::map<std::string, std::string>& ackdict = podata->ackdict;
				char* p = (char*)_stream.begin();
				char control[10] = { 0 };
				char docname[11] = { 0 };
				memcpy_s(control, 8, p + 1, 8);
				memcpy_s(docname, 10, p + 8, 10);

				std::string scontrol = control;
				std::string sdocname = docname;
				std::string ack = "";
				std::string content = p;

				scontrol = util::trim(scontrol);
				sdocname = util::trim(sdocname);

				if (scontrol == "CLOD")
				{
					bool found = true;
					if (ackdict.find(sdocname) == ackdict.end())
					{
						found = _importAckData(sdocname);
					}
					if (found)
						ack = ackdict[sdocname];
					if (0 == ack.length())
						ackdict.erase(sdocname);
				}
				else if (scontrol == "CSAV") {
					bool found = true;
					if (ackdict.find(sdocname) == ackdict.end())
					{
						found = _importAckData(sdocname);
					}
					if (found)
						ack = ackdict[sdocname];
					if (0 == ack.length())
						ackdict.erase(sdocname);
					else if (found)
					{
						bool bsetval(false);
						_bromcr.set(ack);
						std::istringstream iss(content);
						std::string line;
						while (std::getline(iss, line))
						{
							if (line[0] == 'C' || line[0] == 'c')
							{
								std::vector<std::string> v = util::split(line, ",");
								if (v.size() > 0)
								{
									std::string key = std::move(v[0]);
									if (_bromcr.set_val(key, v[1]))
										bsetval = true;
								}
							}
						}
						if (bsetval)
						{
							std::string tmp;
							_bromcr.get(tmp);
							if (tmp.length() > 0)
								ackdict[sdocname] = tmp;
						}
					}
				}
				*size = ack.length();
				if (*size == 0)
				{
					char* tmp = "%Unknow resp%";
					*size = strlen(tmp) + 1;
					*buf = (char*)malloc(*size);
					memset(*buf, 0, *size);
					memcpy_s(*buf, *size, tmp, *size);
				}
				else {
					*buf = (char*)malloc(*size);
					memcpy_s(*buf, *size, ack.c_str(), *size);
				}
				_stream.clear();
			}
		}
	private:		
		wastream _stream;
		bool _importAckData(std::string key);
		broMcr _bromcr;
		int _port;
		static std::map<int, broPoData*> _poData;
		static CriticalSession _poLock;
	};
}

