#pragma once
#include "stdafx.h"

namespace wa
{
	class wastream
	{
	private:
		Byte* _begin;
		Byte* _current;
		int _size;
	public:
		wastream(const int payloadSize) :_begin(NULL), _current(NULL), _size(0)
		{
			init(payloadSize);
		}
		~wastream()
		{
			if (_begin)
				delete[] _begin;
		}

		void init(const int payloadSize)
		{
			_size = payloadSize;
			_begin = new Byte[_size];
			memset(_begin, 0, _size * sizeof(Byte));
			_current = begin();
		}

		void clear()
		{
			memset(_begin, 0, _size * sizeof(Byte));
			_current = begin();
		}

		Byte* begin()
		{
			return _begin;
		}
		Byte* end()
		{
			return _begin + _size;
		}

		int size()
		{
			return _current - begin();
		}

		int capacity()
		{
			return _size;
		}

		void capacity(int len)
		{
			Byte* ed = end();
			Byte* st = begin();
			Byte* oldBegin = _begin;
			int remain = ed - _current;
			if (remain < len)
			{
				int validDataSize = _current - st;
				int append = (len - remain) * 2;
				int currentSize = ed - st;
				init(currentSize + append);
				Byte* newBegin = begin();
				memcpy_s(newBegin, validDataSize, st, validDataSize);
				delete[] oldBegin;
				_current = newBegin + validDataSize;
			}
		}

		void write(Byte* data, int len)
		{
			capacity(len);
			memcpy_s(_current, len, data, len);
			_current += len;
		}

		int read(Byte* Dst, int n)
		{
			Byte* st = begin();
			Byte* ed = end();
			int validDataSize = _current - st;
			if (validDataSize > 0 &&
				validDataSize >= n)
			{
				memcpy_s(Dst, n, st, n);
				drop(n);
				return n;
			}
			return 0;
		}

		bool drop(int nBytes)
		{
			int n = nBytes;
			Byte* st = begin();
			Byte* ed = end();
			int validDataSize = _current - st;
			if (validDataSize > 0 &&
				validDataSize >= n)
			{
				int remain = validDataSize - n;
				Byte* newBegin = st + n;
				for (int i = 0; i < remain; i++)
				{
					*(st + i) = *(newBegin + i);
				}
				_current = st + remain;
				memset(_current, 0, ed - _current);
				return true;
			}
			return false;
		}
	};
}