#include "stdafx.h"
#include "broConn.h"
#include <string>
#include <iostream>
#include <sstream>
#include  <io.h>  
#include  <stdio.h>  
#include  <stdlib.h>  
#include <fstream>

namespace wa
{
	std::map<int, broPoData*> broConn::_poData;
	CriticalSession broConn::_poLock;

	int broConn::DeviceType = 0;

	broConn::broConn(int port):_stream(1024), _port(port)
	{		
		THREAD_LIB_LOCK_CRITICAL_SESSION(_poLock)
		if (_poData.find(_port) == _poData.end())
			_poData[_port] = new broPoData();	
	}

	broConn::~broConn()
	{
	}

	void getCurPath(char* path, int n)
	{
		char* p = new char[n];
		memset(p, 0, n);
		GetModuleFileName(NULL, p, 2048);
		int m = strlen(p);
		for (int i = m; i >= 0; i--)
		{
			if (p[i] == '\\')
			{
				p[i] = 0;
				break;
			}
		}
		memmove(path, p, n);
		delete[] p;
	}

	bool broConn::_importAckData(std::string key)
	{
		char curpath[MAX_PATH] = { 0 }, bankdr[MAX_PATH] = { 0 };
		getCurPath(curpath, MAX_PATH);

		if (broConn::DeviceType == 0)
			sprintf_s(bankdr, "%s\\ack_s\\b00\\", curpath);
		else if (broConn::DeviceType == 2)
			sprintf_s(bankdr, "%s\\ack_s\\c00sp\\", curpath);
		else
			sprintf_s(bankdr, "%s\\ack_s\\c00\\", curpath);
		std::string ackFilesDir = bankdr;
		std::string fileName = key + ".txt";

		std::string ff = ackFilesDir + fileName;
		if ((_access(ff.c_str(), 0)) != -1)
		{
			broPoData* podata = NULL;
			{
				THREAD_LIB_LOCK_CRITICAL_SESSION(_poLock)				
				if (_poData.find(_port) == _poData.end())
					_poData[_port] = new broPoData();				
				podata = _poData[_port];			
				std::map<std::string, std::string>& ackdict = podata->ackdict;
				ackdict[key] = "";
				std::ifstream input(ff.c_str());
				std::string line;
				int found = 0, endcnt = 3;

				std::string ssstr("");

				while (std::getline(input, line))
				{
					if (std::string::npos != line.find_first_of("%"))
					{
						++found;
					}

					if (found == 1 &&
						(std::string::npos == line.find("CLOD")))
					{
						endcnt = 1;
					}

					if (found >= endcnt)
					{
						ssstr.append(line);
						ssstr.append("\n");
					}
				}
				ackdict[key] = util::trim(ssstr);

				//MessageBox(NULL, curpath, ff.c_str(), 0);
				//walog::out << _ackdict[key];
				//walog::flush();
			}

			return true;
		}
		return false;
	}
}
