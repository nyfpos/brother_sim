#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "webserver.h"
#include "uv.h"
#include "http_parser.h"
#include "http_request_handler.h"

//#define DEBUG_HEADERS

namespace waweb
{
	static int webport;
	static uv_loop_t* uv_loop;
	static uv_tcp_t server;
	static http_parser_settings parser_settings;	

	static struct message messages;
	static int currently_parsing_eof = 0;

	http_request_handler* request_handler;

	size_t strlncat(
		char *dst,
		size_t len,
		const char *src,
		size_t n)
	{
		const char * const orig_dst = dst;

		while (*dst != 0 && len > 0) {
			dst++;
			len--;
		}

		if (len > 0) {
			len--;
			while (*src != 0 && len > 0 && n > 0) {
				*dst++ = *src++;
				len--;
				n--;
			}
			*dst = 0;
		}
		len = strlen(src);
		if (n < len)
			len = n;
		return (dst - orig_dst) + len;
	}



	void on_close(uv_handle_t* handle) {
		client_t* client = (client_t*)handle->data;

		//printf("[web] connection closed\n");

		free(client);
	}

	void on_alloc(uv_handle_t* handle, size_t suggested_size, uv_buf_t* buf) {
		*buf = uv_buf_init((char*)malloc(suggested_size), suggested_size);
	}

	void on_read(uv_stream_t* tcp, ssize_t nread, const uv_buf_t* buf) {
		size_t parsed;

		client_t* client = (client_t*)tcp->data;

		if (nread >= 0) {
			parsed = http_parser_execute(
				&client->parser, &parser_settings, buf->base, nread);
			if ((ssize_t)parsed < nread) {
				LOG_ERROR("parse error");
				uv_close((uv_handle_t*)&client->handle, on_close);
			}
		}
		else {
			if (nread != UV_EOF) {
				UVERR(nread, "read");
			}
			else {
				printf("[web] read UV_EOF\n");
			}
			uv_close((uv_handle_t*)&client->handle, on_close);
		}

		free(buf->base);
	}

	void on_connect(uv_stream_t* server_handle, int status) {
		CHECK(status, "connect");

		int r;

		assert((uv_tcp_t*)server_handle == &server);

		client_t* client = (client_t*)malloc(sizeof(client_t));

		//printf("[web] new connection\n");

		uv_tcp_init(uv_loop, &client->handle);
		http_parser_init(&client->parser, HTTP_REQUEST);

		client->parser.data = client;
		client->handle.data = client;

		r = uv_accept(server_handle, (uv_stream_t*)&client->handle);
		CHECK(r, "accept");

		uv_read_start((uv_stream_t*)&client->handle, on_alloc, on_read);
	}

	void after_write(uv_write_t* req, int status) {
		CHECK(status, "write");
		uv_buf_t* buf = (uv_buf_t*)req->data;
		free(buf->base);
		free(buf);
		uv_close((uv_handle_t*)req->handle, on_close);
	}

	int message_begin_cb(http_parser *p)
	{
		memset(&messages, 0, sizeof(message));

		messages.message_begin_cb_called = TRUE;
		return 0;
	}
	int header_field_cb(http_parser *p, const char *buf, size_t len)
	{
		struct message *m = &messages;

		if (m->last_header_element != FIELD)
			m->num_headers++;

		if (m->num_headers > MAX_HEADERS)
			m->num_headers = MAX_HEADERS;

		strlncat(m->headers[m->num_headers - 1][0],
			sizeof(m->headers[m->num_headers - 1][0]),
			buf,
			len);

		m->last_header_element = FIELD;

		return 0;
	}

	int header_value_cb(http_parser *p, const char *buf, size_t len)
	{
		struct message *m = &messages;

		strlncat(m->headers[m->num_headers - 1][1],
			sizeof(m->headers[m->num_headers - 1][1]),
			buf,
			len);

		m->last_header_element = VALUE;

		return 0;
	}

	int request_url_cb(http_parser *p, const char *buf, size_t len)
	{
		strlncat(messages.request_url,
			sizeof(messages.request_url),
			buf,
			len);
		return 0;
	}

	int response_status_cb(http_parser *p, const char *buf, size_t len)
	{
		messages.status_cb_called = TRUE;

		strlncat(messages.response_status,
			sizeof(messages.response_status),
			buf,
			len);
		return 0;
	}

	int chunk_header_cb(http_parser *p)
	{
		int chunk_idx = messages.num_chunks;
		messages.num_chunks++;
		if (chunk_idx < MAX_CHUNKS) {
			messages.chunk_lengths[chunk_idx] = (int)p->content_length;
		}

		return 0;
	}

	int chunk_complete_cb(http_parser *p)
	{
		/* Here we want to verify that each chunk_header_cb is matched by a
		* chunk_complete_cb, so not only should the total number of calls to
		* both callbacks be the same, but they also should be interleaved
		* properly */
		assert(messages.num_chunks ==
			messages.num_chunks_complete + 1);

		messages.num_chunks_complete++;
		return 0;
	}

	void check_body_is_final(const http_parser *p)
	{
		if (messages.body_is_final) {
			fprintf(stderr, "\n\n *** Error http_body_is_final() should return 1 "
				"on last on_body callback call "
				"but it doesn't! ***\n\n");
			assert(0);
			abort();
		}
		messages.body_is_final = http_body_is_final(p);
	}

	int body_cb(http_parser *p, const char *buf, size_t len)
	{
		strlncat(messages.body,
			sizeof(messages.body),
			buf,
			len);
		messages.body_size += len;
		check_body_is_final(p);
		// printf("[web] body_cb: '%s'\n", requests[num_messages].body);
		return 0;
	}

	int headers_complete_cb(http_parser *p)
	{
		messages.method = (http_method)p->method;
		messages.status_code = p->status_code;
		messages.http_major = p->http_major;
		messages.http_minor = p->http_minor;
		messages.headers_complete_cb_called = TRUE;
		messages.should_keep_alive = http_should_keep_alive(p);
		return 0;
	}

	int message_complete_cb(http_parser *p)
	{
		if (messages.should_keep_alive != http_should_keep_alive(p))
		{
			fprintf(stderr, "\n\n *** Error http_should_keep_alive() should have same "
				"value in both on_message_complete and on_headers_complete "
				"but it doesn't! ***\n\n");
			assert(0);
			abort();
		}

		if (messages.body_size &&
			http_body_is_final(p) &&
			!messages.body_is_final)
		{
			fprintf(stderr, "\n\n *** Error http_body_is_final() should return 1 "
				"on last on_body callback call "
				"but it doesn't! ***\n\n");
			assert(0);
			abort();
		}

		messages.message_complete_cb_called = TRUE;

		messages.message_complete_on_eof = currently_parsing_eof;

		client_t* client = (client_t*)p->data;
#ifdef DEBUG_HEADERS	
		printf("[web] request:%s-------------\n", messages.request_url);
		for (int i = 0; i < MAX_HEADERS; i++)
		{
			if (0 == strlen(messages.headers[i][0]))
			{
				break;
			} else {
				printf("[web] headers[%d]: [0]:%s  [1]:%s\n", i, messages.headers[i][0], messages.headers[i][1]);
			}
		}		
		printf("[web] ----------------------\n");
#endif		
		char* resp =NULL;
		int size = 0;
		request_handler->get_resp(messages, &resp, &size);

		if (size > 0 && resp != NULL)
		{
			uv_buf_t* buf = (uv_buf_t*)malloc(sizeof(uv_buf_t));
			memset(buf, 0, sizeof(uv_buf_t));
			*buf = uv_buf_init(resp, size);
			(client->write_req).data = buf;
			uv_write(
				&client->write_req,
				(uv_stream_t*)&client->handle,
				buf,
				1,
				after_write);
		}
		else {
			printf("[web] failed to get resp: %s\n", messages.request_url);
		}
		return 1;
	}

	/*
	int on_headers_complete(http_parser* parser) {
		client_t* client = (client_t*)parser->data;

		printf("[web] [ %5d ] http message parsed", client->request_num);

		uv_write(
			&client->write_req,
			(uv_stream_t*)&client->handle,
			&resbuf,
			1,
			after_write);

		return 1;
	}*/

	void on_work(uv_work_t *work)
	{
		uv_loop = (uv_loop_t*)malloc(sizeof(uv_loop_t));
		uv_loop_init(uv_loop);

		int r;

		parser_settings.on_message_begin = message_begin_cb;
		parser_settings.on_header_field = header_field_cb;
		parser_settings.on_header_value = header_value_cb;
		parser_settings.on_url = request_url_cb;
		parser_settings.on_status = response_status_cb;
		parser_settings.on_body = body_cb;
		parser_settings.on_headers_complete = headers_complete_cb;
		parser_settings.on_message_complete = message_complete_cb;
		parser_settings.on_chunk_header = chunk_header_cb;
		parser_settings.on_chunk_complete = chunk_complete_cb;

		r = uv_tcp_init(uv_loop, &server);
		CHECK(r, "bind");

		struct sockaddr_in6 addr;
		uv_ip6_addr("::/128", webport, &addr);

		r = uv_tcp_bind(&server, (const struct sockaddr*)&addr, 0);
		CHECK(r, "bind");
		uv_listen((uv_stream_t*)&server, 128, on_connect);

		printf("[web] webserver listening on port %d\n", webport);

		uv_run(uv_loop, UV_RUN_DEFAULT);
		uv_loop_close(uv_loop);
		uv_close((uv_handle_t*)&server, NULL);
	}

	void close_work(uv_work_t *work, int status)
	{
		free(work);
	}

	void start(uv_loop_t* loop, int port)
	{
		webport = port;
		uv_work_t* work = (uv_work_t*)malloc(sizeof(uv_work_t));
		memset(work, 0, sizeof(uv_work_t));
		uv_queue_work(loop, work, on_work, close_work);
	}
}