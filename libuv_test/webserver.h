#pragma once
#include "uv.h"
#include "http_parser.h"

namespace waweb
{

class http_request_handler;

#define MAX_HEADERS 13
#define MAX_ELEMENT_SIZE 2048
#define MAX_CHUNKS 16

#define CHECK(r, msg) \
  if (r < 0) { \
    fprintf(stderr, "%s: %s\n", msg, uv_strerror(r)); \
    exit(1); \
  }
#define UVERR(err, msg) fprintf(stderr, "%s: %s\n", msg, uv_strerror(err))
#define LOG_ERROR(msg) puts(msg);

	typedef struct
	{	
		void(*start)(int port);
		void(*stop)(int port);
		void(*list)(std::vector<int>& srvs);
	} iwa_webapi;

	typedef struct {
		uv_tcp_t handle;
		http_parser parser;
		uv_write_t write_req;
	} client_t;
		
	enum elem_type
	{ 
		NONE = 0, 
		FIELD, 
		VALUE 
	};
	struct message 
	{
		const char *name; // for debugging purposes
		const char *raw;
		enum http_parser_type type;
		enum http_method method;
		int status_code;
		char response_status[MAX_ELEMENT_SIZE];
		char request_path[MAX_ELEMENT_SIZE];
		char request_url[MAX_ELEMENT_SIZE];
		char fragment[MAX_ELEMENT_SIZE];
		char query_string[MAX_ELEMENT_SIZE];
		char body[MAX_ELEMENT_SIZE];
		size_t body_size;
		const char *host;
		const char *userinfo;
		uint16_t port;
		int num_headers;
		elem_type last_header_element;
		char headers[MAX_HEADERS][2][MAX_ELEMENT_SIZE];
		int should_keep_alive;

		int num_chunks;
		int num_chunks_complete;
		int chunk_lengths[MAX_CHUNKS];

		const char *upgrade; // upgraded body

		unsigned short http_major;
		unsigned short http_minor;

		int message_begin_cb_called;
		int headers_complete_cb_called;
		int message_complete_cb_called;
		int status_cb_called;
		int message_complete_on_eof;
		int body_is_final;
	};

	void start(uv_loop_t* loop, int webport);

	extern http_request_handler* request_handler;
}