#include "stdafx.h"
#include "test_fs.h"
namespace test_fs
{
	struct _fs_param
	{
		uv_fs_t open_req;
		uv_fs_t read_req;
		uv_fs_t write_req;
		uv_fs_t close_req;
		uv_buf_t iov;
	};

	//static 

	const char* path1 = "c:\\test1.txt";
	const char* path2 = "c:\\test2.txt";
	char* base = NULL;

	void on_read(uv_fs_t *req);
	void on_write(uv_fs_t *req);

	void start()
	{
		_fs_param* para = (_fs_param*)malloc(sizeof(_fs_param));

		unsigned int len = 1024 * 1024 * sizeof(char);
		base = (char*)malloc(len);
		memset(base, 0, len);
		if (base)
		{
			para->iov = uv_buf_init(base, len);
			para->open_req.data = (void*)para;
			uv_fs_open(uv_default_loop(), &(para->open_req), path1, _O_RDONLY, _S_IREAD, NULL);
			if ((para->open_req).result < 0)
			{
				printf("fail to open file %s\n", path1);
			}
			else {
				uv_fs_t *req = &(para->open_req);
				_fs_param* para = (_fs_param*)req->data;
				(para->read_req).data = (void*)para;
				uv_fs_read(uv_default_loop(), &(para->read_req), req->result,
					&(para->iov), 1, -1, on_read);
			}
		}
		else {
			printf("memory not enough.\n");
		}

		uv_run(uv_default_loop(), UV_RUN_DEFAULT);

		if (para)
			free(para);
		if (base)
			free(base);
	}


	void on_read(uv_fs_t *req)
	{
		uv_fs_req_cleanup(req);
		_fs_param* para = (_fs_param*)req->data;
		if (req->result < 0)
		{
			printf("read error\n");
		}
		else if (req->result == 0) {
			printf("no ata file close..");
			uv_fs_close(uv_default_loop(), &(para->close_req),
				(para->open_req).result, NULL);
		}
		else {
			printf("[%d]\n %s\n", req->result, (para->iov).base);
			uv_fs_close(uv_default_loop(),
				&(para->close_req), (para->open_req).result, NULL);

			req = &(para->open_req);
			req->data = (void*)para;
			uv_fs_open(uv_default_loop(), req, path2,
				_O_RDWR | _O_CREAT, _S_IREAD | _S_IWRITE, NULL);
			_fs_param* para = (_fs_param*)req->data;
			(para->write_req).data = (void*)para;
			if (req->result < 0)
			{
				printf("Fail to open file to write\n");
			}
			else {
				unsigned int len = (para->read_req).result;
				printf("read data len:%d\n", len);
				(para->iov).len = len;
				uv_fs_write(uv_default_loop(), &(para->write_req), req->result,
					&(para->iov), (para->iov).len, -1, on_write);
			}
		}
	}

	void on_write(uv_fs_t *req)
	{
		uv_fs_req_cleanup(req);
		_fs_param* para = (_fs_param*)req->data;
		if (req->result < 0)
		{
			printf("write error\n");
		}
		printf("write %d\n", req->result);
		printf("write close..\n");
		uv_fs_close(uv_default_loop(), &(para->close_req),
			(para->open_req).result, NULL);
	}

	/*
	uv_fs_t is a subclass of uv_req_t.
	struct uv_fs_s {
	UV_REQ_FIELDS
	uv_fs_type fs_type;
	uv_loop_t* loop;
	uv_fs_cb cb;
	ssize_t result;
	void* ptr;
	const char* path;
	uv_stat_t statbuf;  // Stores the result of uv_fs_stat() and uv_fs_fstat()
	UV_FS_PRIVATE_FIELDS
	};
	*/

	/*
	int64_t counter = 0;
	void wait_for_a_while(uv_idle_t* handle)
	{
	++counter;
	if (counter >= 10e6)
	{
	uv_idle_stop(handle);
	}
	}
	*/
	/*
	int main()
	{
	// custom uv_loop
	uv_loop_t *loop = (uv_loop_t *)malloc(sizeof(uv_loop_t));
	uv_loop_init(loop);

	printf("Now quitting.\n");
	uv_run(loop, UV_RUN_DEFAULT);

	uv_loop_close(loop);
	free(loop);

	uv_idle_t idler;

	uv_idle_init(uv_default_loop(), &idler);
	uv_idle_start(&idler, wait_for_a_while);

	printf("Idling...\n");
	uv_run(uv_default_loop(), UV_RUN_DEFAULT);

	uv_loop_close(uv_default_loop());
	}
	*/

}
