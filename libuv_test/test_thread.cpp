#include "stdafx.h"
#include "test_thread.h"

#define FIB_UNTIL 50
#include "time.h"
namespace test_thread
{
	int fib_(int n)
	{
		if (n == 1 || n == 2)
		{
			return 1;
		}
		else if (n == 0)
		{
			return 0;
		}
		else {
			return fib_(n - 1) + fib_(n - 2);
		}
	}

	void fib(uv_work_t *req) 
	{
		srand((unsigned int)time(NULL));
		int x = rand();
		int n = *(int *)req->data;
		if (rand() % 2)
			Sleep(1);
		else
			Sleep(3);
		long fib = fib_(n);
		printf("%dth fibonacci is %lu\n", n, fib);
	}

	void after_fib(uv_work_t *req, int status) {
		printf("Done calculating %dth fibonacci\n", *(int *)req->data);
	}

	void start()
	{
		uv_loop_t* loop = uv_default_loop();

		int data[FIB_UNTIL];
		uv_work_t req[FIB_UNTIL];
		int i;
		for (i = 0; i < FIB_UNTIL; i++) {
			data[i] = i;
			req[i].data = (void *)&data[i];
			uv_queue_work(loop, &req[i], fib, after_fib);
		}

		uv_run(loop, UV_RUN_DEFAULT);
	}
}