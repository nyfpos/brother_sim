#pragma once

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <functional>
#include <cctype>

class util
{
public:
	util();
	~util();

	static inline std::string getCurPath()
	{
		std::string s;
		int n = 2048;
		char* p = new char[n];
		memset(p, 0, n);
		GetModuleFileName(NULL, p, 2048);
		int m = strlen(p);
		for (int i = m; i >= 0; i--)
		{
			if (p[i] == '\\')
			{
				p[i] = 0;
				break;
			}
		}
		s = p;
		delete[] p;
		return std::move(s);
	}

	static inline std::vector<std::string> split(std::string s, const std::string& delimiter)
	{
		std::vector<std::string> ret;
		size_t pos = 0;
		std::string token;
		while ((pos = s.find(delimiter)) != std::string::npos)
		{
			token = s.substr(0, pos);
			ret.push_back(std::move(token));
			s.erase(0, pos + delimiter.length());
		}
		ret.push_back(std::move(s));
		return std::move(ret);
	}

	static inline void replaceAll(std::string& str, const std::string& from, const std::string& to)
	{
		if (from.empty())
			return;
		size_t start_pos = 0;
		while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
			str.replace(start_pos, from.length(), to);
			start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
		}
	}

	template <typename T>
	static inline std::basic_string<T>& ToUppercase(std::basic_string<T>& str)
	{
		std::transform(str.begin(), str.end(), str.begin(), ::toupper);
		return str;
	}

	template <typename T>
	static inline std::basic_string<T>& ToLowercase(std::basic_string<T>& str)
	{
		std::transform(str.begin(), str.end(), str.begin(), ::tolower);
		return str;
	}

	// trim from start
	template <typename T>
	static inline std::basic_string<T>& ltrim(std::basic_string<T> &s) {
		s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
		return s;
	}

	// trim from end
	template <typename T>
	static inline std::basic_string<T> &rtrim(std::basic_string<T> &s) {
		s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
		return s;
	}

	// trim from both ends
	template <typename T>
	static inline std::basic_string<T> &trim(std::basic_string<T> &s) {
		return ltrim(rtrim(s));
	}

	static inline std::string c_str2hex(const char* data, int n)
	{
		std::stringstream ss;
		for (int i = 0; i< n; ++i)
			ss << std::hex << (int)data[i];
		return std::move(ss.str());
	}

	template <typename T>
	static inline std::basic_string<T> toHex(int to_convert)
	{
		std::basic_string<T> result;
		std::basic_stringstream<T> ss;
		ss << std::hex << to_convert;
		ss >> result;
		return result;
	}

	static inline std::wstring s2ws(const std::string& s)
	{
		int len;
		int slength = (int)s.length() + 1;
		len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
		std::wstring r(len, L'\0');
		MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, &r[0], len);
		return r;
	}

	static inline std::string ws2s(const std::wstring& s)
	{
		int len;
		int slength = (int)s.length() + 1;
		len = WideCharToMultiByte(CP_ACP, 0, s.c_str(), slength, 0, 0, 0, 0);
		std::string r(len, '\0');
		WideCharToMultiByte(CP_ACP, 0, s.c_str(), slength, &r[0], len, 0, 0);
		return r;
	}

	template<int N>
	static inline void ConvertCharToWChar(const char* orig, wchar_t(&dest)[N])
	{
		size_t converted = 0;
		mbstowcs_s<N>(&converted, dest, orig, _TRUNCATE);
	}
	template<int N>
	static inline void ConvertWCharToChar(const wchar_t* org, char(&dest)[N])
	{
		size_t converted = 0;
		size_t ret = wcstombs_s(&converted, dest, org, _TRUNCATE);
		if (ret == N) dest[N - 1] = '\0';
	}
};

