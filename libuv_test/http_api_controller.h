#pragma once
#include "webserver.h"
#include <vector>
namespace waweb
{
	class http_api_controller
	{
	public:
		http_api_controller(iwa_webapi srvApi);
		~http_api_controller();
		bool get_api_resp(struct message& ctx, char** outary, int* outsize);
	private:
		iwa_webapi _srvApi;
		bool _api_list(struct message& ctx, char** outary, int* outsize);
		bool _api_start(struct message& ctx, char** outary, int* outsize);
		bool _api_stop(struct message& ctx, char** outary, int* outsize);
		std::vector<int> _srvs;
	};
}