#include "stdafx.h"
#include "http_request_handler.h"
#include "webserver.h"
#include "util.h"
#include <fstream>
#include <string>

namespace waweb
{
	static const std::string rootDir = "/public";

	static const char* resp_templ = "HTTP/1.1 200 OK\r\n"
		"Content-Type: %s\r\n"
		"Content-Length: %d\r\n"
		"Content-Transfer-Encoding: %s\r\n"
		"\r\n";

	/*
	Type application
	application/EDI-X12
	application/EDIFACT
	application/javascript
	application/octet-stream
	application/ogg
	application/pdf
	application/xhtml+xml
	application/x-shockwave-flash
	application/json
	application/ld+json
	application/xml
	application/zip

	Type audio
	audio/mpeg
	audio/x-ms-wma
	audio/vnd.rn-realaudio
	audio/x-wav

	Type image
	image/gif
	image/jpeg
	image/png
	image/tiff
	image/vnd.microsoft.icon
	image/x-icon
	image/vnd.djvu
	image/svg+xml

	Type multipart
	multipart/mixed
	multipart/alternative
	multipart/related (using by MHTML (HTML mail).)

	Type text
	text/css
	text/csv
	text/html
	text/javascript (obsolete)
	text/plain
	text/xml

	Type video
	video/mpeg
	video/mp4
	video/quicktime
	video/x-ms-wmv
	video/x-msvideo
	video/x-flv
	video/webm

	Type vnd :
	application/vnd.oasis.opendocument.text
	application/vnd.oasis.opendocument.spreadsheet
	application/vnd.oasis.opendocument.presentation
	application/vnd.oasis.opendocument.graphics
	application/vnd.ms-excel
	application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
	application/vnd.ms-powerpoint
	application/vnd.openxmlformats-officedocument.presentationml.presentation
	application/msword
	application/vnd.openxmlformats-officedocument.wordprocessingml.document
	application/vnd.mozilla.xul+xml
	*/

	void make_response(std::string& response, const char* contentType, int len, int content_len, const char* encode, std::string content="")
	{
		char* tmp = new char[len];
		memset(tmp, 0, len);
		sprintf_s(tmp, len, response.c_str(), contentType, content_len, encode);
		response = tmp;
		delete[] tmp;
		if (content.length() > 0)
		{
			response.append(content.c_str());
		}
	}

	bool get_content_file(std::string file, char** outary, int* outsize)
	{
		bool ok(false);
		int bary_sz = 0;
		char* bary = NULL;
		FILE *fp = NULL;
		errno_t err;
		if (0 == (err = fopen_s(&fp, file.c_str(), "rb")))
		{
			fseek(fp, 0L, SEEK_END);
			bary_sz = ftell(fp);
			fseek(fp, 0L, SEEK_SET);
			bary = (char*)malloc(bary_sz * sizeof(char));
			memset(bary, 0, bary_sz);
			int ss = fread(bary, 1, bary_sz, fp);
			if (ss != bary_sz)
			{
				printf("[web] read image failed\n");
				free(bary);
			} else {
				ok = true;
				*outary = bary;
				*outsize = bary_sz;
			}
			fclose(fp);
		}
		else {
			printf("[web] failed to open file %s\n", file.c_str());
		}
		return ok;
	}

	http_request_handler::http_request_handler(iwa_webapi srvApi):_api_contrl(srvApi)
	{
	}


	http_request_handler::~http_request_handler()
	{
	}

	void http_request_handler::get_resp(struct message& ctx, char** resp, int* size)
	{
		int bary_sz = 0;
		char* bary = NULL;
		std::string response = resp_templ;
		int i = 0;
		for (i = 0; i < MAX_HEADERS; i++)
		{
			if (0 == _stricmp(ctx.headers[i][0], "Accept"))
			{
				break;
			}
		}

		bool ok(false);
		if (i < MAX_HEADERS)
		{			
			bool bjavascript(false), bimage(false);
			auto types = util::split(ctx.headers[i][1], "/");
			std::string curpath = std::move(util::getCurPath()) + rootDir;
			std::string url = ctx.request_url;
			std::size_t jsfound = url.find_last_of(".");
			if (jsfound != std::string::npos)
			{
				auto ext = util::ToLowercase(url.substr(jsfound));
				if (ext == ".js")
					bjavascript = true;
				else if (ext == ".ico" || ext == "tif" ||
					     ext == ".tiff" || ext == "gif" || 
					     ext == ".jpeg" || ext == "jpg" || 
					     ext == ".png")
					bimage = true;
			}
			if (types.size() >= 2)
			{
				ok = true;
				types[0] = util::ToLowercase(util::trim(types[0]));
				types[1] = util::ToLowercase(util::trim(types[1]));
				bool appnojson =  types[0] == "application" && types[1].find("json") == std::string::npos;
				if (!bimage && !bjavascript && 
					(types[0] == "text" || types[0] == "*" || appnojson || url == "/"))
				{
					ok = false;
					if (url == "/")
						url = "/index.html";
					url = curpath + url;
					if (get_content_file(url, &bary, &bary_sz))
					{
						std::string resptype = "text/html";
						if (types[1].find("css") != std::string::npos)				
							resptype = "text/css";
						else if (types[1].find("plain") != std::string::npos)
							resptype = "text/plain";
						else if (types[1].find("xml") != std::string::npos)
							resptype = "text/xml";
						else if (types[1].find("csv") != std::string::npos)
							resptype = "text/csv";						
						int len = response.length() + strlen("text/html") + 10;
						make_response(response, resptype.c_str(), len, bary_sz, "8bit");
						ok = true;
					}
				} else if (types[0] == "image" || bimage) {
					ok = false;
					url = curpath + url;
					if (get_content_file(url, &bary, &bary_sz))
					{
						int len = response.length() + strlen("image/*") + 10;
						make_response(response, "image/*", len, bary_sz, "binary");
						ok = true;
					}
				} else if (!appnojson || bjavascript)
				{
					ok = false;
					auto apptype = util::ToLowercase(util::trim(types[1]));
					if (apptype.find("javascript") != std::string::npos || bjavascript)
					{
						url = curpath + url;
						if (get_content_file(url, &bary, &bary_sz))
						{
							int len = response.length() + strlen("application/javascript") + 10;
							make_response(response, "application/javascript", len, bary_sz, "8bit");
							ok = true;
						}
					} else if (_api_contrl.get_api_resp(ctx, &bary, &bary_sz))
					{
						int len = response.length() + strlen("application/json; charset=utf-8") + 10;
						make_response(response, "application/json; charset=utf-8", len, bary_sz, "binary");
						ok = true;
					}
				} else {
					printf("[web] unknown type:%s\n", types[0].c_str());
					ok = false;
				}
			} else {
				printf("[web] types.size():%d", types.size());
			}
		} else {
			printf("[web] not found Accept!!");
			for (int i = 0; i < MAX_HEADERS; i++)
			{
				if (0 == strlen(ctx.headers[i][0]))
				{
					break;
				}
				else {
					printf("[web] headers[%d]: [0]:%s  [1]:%s\n", i, ctx.headers[i][0], ctx.headers[i][1]);
				}
			}
			printf("[web] url:%s %s\n", ctx.request_url, ctx.request_path);
		}
		if (!ok) {
			printf("[web]not support content-type:%s\n", ctx.headers[i][1]);
			int len = response.length() + 280;
			make_response(response, "text/plain", len, 25, "8bit", "Not available resources!\n");
		}
		char* p = NULL;
		int sz = 0;
		sz = response.length() + 1;
		p = (char*)malloc(sizeof(char)*(sz + bary_sz));
		memset(p, 0, sz + bary_sz);
		memcpy_s(p, sz, response.c_str(), sz);
		if (bary != NULL && bary_sz)
		{
			memcpy_s(p +sz-1, bary_sz, bary, bary_sz);
			free(bary);
		}
		*size = sz + bary_sz;
		*resp = p;
	}
}