#include "stdafx.h"
#include <queue>
#include "wa_tcp_server.h"

namespace wa
{	
	make_wa_conn wa_tcp_server::makeConn = NULL;
	del_wa_conn wa_tcp_server::delConn = NULL;

	wa_tcp_server::wa_tcp_server(uv_loop_t* loop):
		_loop(loop)
	{
	}

	wa_tcp_server::~wa_tcp_server()
	{
	}

	req_uv_write* wrap_write_req(req_uv_write *write_req, uv_buf_t* uvbuf,
		                uv_tcp_t* client, iwa_tcp_conn* iwa_conn)
	{
		memset(write_req, 0, sizeof(req_uv_write));
		write_req->buf = uvbuf;
		write_req->client = client;
		write_req->iwa_conn = iwa_conn;
		return write_req;
	}

	void rm_conn_worker(req_uv_work* wr, req_conn* conn)
	{
		if (NULL != wr && NULL != conn)
		{
			req_conn* p = wr->conns;
			if (NULL != p)
			{
				if ((int)p == (int)conn)
				{
					wr->conns = p->next;
				} else {
					req_conn* pre = p;
					p = p->next;
					while (NULL != p)
					{
						if ((int)p == (int)conn)
						{
							pre->next = p->next;
							break;
						}
						pre = p;
						p = p->next;
					}
				}
			}
		}
	}

	void add_conn_worker(req_uv_work* wr, req_conn* conn)
	{
		if (NULL == wr->conns)
		{
			wr->conns = conn;
		}
		else {
			req_conn* p = wr->conns;
			while (p->next != NULL)
				p = p->next;
			p->next = conn;
		}
		conn->next = NULL;
	}

	void free_client(uv_handle_t* handle)
	{
		req_conn *req = (req_conn*)handle;
		rm_conn_worker(req->work, req);
		wa_tcp_server::delConn(req->iwa_conn);
		free(req);
	}

	void close_conns(req_uv_work* wr)
	{
		req_conn* p = wr->conns;
		while (p != NULL)
		{
			req_conn* req = p;
			p = p->next;
			uv_tcp_t* client = &req->handle;
			uv_read_stop((uv_stream_t*)client);
			req->iwa_conn->set_conn_stat(Disconnected);
			int fd = client->socket;
			uv_close((uv_handle_t*)client, free_client);
			printf("[on_close_fd] manual close fd:%d\n", fd);
		}
	}

	void on_close_worker(uv_async_t *handle)
	{
		if (handle && handle->data)
		{
			req_uv_work* wr = (req_uv_work*)handle->data;
			close_conns(wr);
			uv_stop(&wr->loop);
		}
	}

	void alloc_buffer(uv_handle_t* handle, size_t suggested_size, uv_buf_t* buf)
	{
		*buf = uv_buf_init((char*)malloc(suggested_size), suggested_size);
	}

	void on_write(uv_write_t *req, int status)
	{
		req_uv_write *write_req = (req_uv_write *)req;
		int fd = write_req->client->socket;
		if (status == -1) {
			printf("[on_write] Write error!\n");
		}				
		//free_client((uv_handle_t*)write_req->client);
		free(write_req->buf->base);
		free(write_req->buf);
		free(write_req);
	}

	void on_read(uv_stream_t* cli, ssize_t nread, const uv_buf_t* buf)
	{
		req_conn *req = (req_conn*)cli;
		uv_tcp_t* client = &req->handle;
		int fd = client->socket;
		if (nread < 0) {
			if (nread != UV_EOF) {
				printf("[on_read] Read error %s! closed fd:%d\n", uv_err_name(nread), fd);
			}
			else {
				printf("[on_read] Read error EOF! closed fd:%d\n", fd);
			}
			uv_close((uv_handle_t*)client, free_client);
		} else if (nread > 0) {			
			// printf("recv data: %d bytes\n", nread);			
			iwa_tcp_conn* conn = req->iwa_conn;
			conn->on_read_some(buf->base, nread);
			if (conn->be_read_done())
			{
				char* wrbuf = NULL;
				int wrsize = 0;
				conn->get_write_buf(&wrbuf, &wrsize);
				if (wrbuf && wrsize > 0)
				{												
					uv_buf_t* uvbuf = (uv_buf_t*)malloc(sizeof(uv_buf_t));
					*uvbuf = uv_buf_init(wrbuf, wrsize);
					req_uv_write *write_req = wrap_write_req((req_uv_write*)malloc(sizeof(req_uv_write)), uvbuf, client, conn);
					uv_write(&write_req->handle, (uv_stream_t*)client, uvbuf, 1, on_write);
				}
			}
		} else {
			printf("[on_read] recv end closed fd:%d\n", fd);
			uv_close((uv_handle_t*)client, free_client);
		}
		if (buf->base) {
			free(buf->base);
		}
	}

	void on_new_connection(uv_stream_t *server, int status)
	{
		if (status < 0) {
			printf("[on_new_connection] error %s\n", uv_strerror(status));
			// error!
			return;
		}
		req_uv_work* wr = (req_uv_work*)server->data;
		req_server* req = (req_server*)server;
		req_conn *client = (req_conn*)malloc(sizeof(req_conn));
		memset(client, 0, sizeof(req_conn));
		uv_tcp_t* cli_hnd = &client->handle;
		client->work = wr;
		client->iwa_conn = wa_tcp_server::makeConn(wr->port);
		client->async = req->async;
		uv_tcp_init(req->loop, cli_hnd);
		if (uv_accept((uv_stream_t*)&req->handle, (uv_stream_t*)cli_hnd) == 0) {
			add_conn_worker(wr, client);
			client->iwa_conn->set_conn_stat(Connected);				
			uv_read_start((uv_stream_t*)cli_hnd, alloc_buffer, on_read);
			printf("[on_new_connection] accept fd:%d ok\n", cli_hnd->socket);
		} else {
			printf("[on_new_connection] closed fd:\n");
			uv_close((uv_handle_t*)cli_hnd, free_client);
		}
	}

	void on_work(uv_work_t *work)
	{
		req_uv_work* wr = (req_uv_work*)work;
		uv_loop_t* loop = &wr->loop;	
		req_server* req = (req_server*)malloc(sizeof(req_server));
		memset(req, 0, sizeof(req_server));
		req->loop = loop;
		req->async = &wr->async;
		req->async->data = wr;
		(req->handle).data = wr;

		uv_async_init(loop, req->async, on_close_worker);
		uv_tcp_init(loop, &req->handle);

		struct sockaddr_in6 addr;
		uv_ip6_addr(wr->ip, wr->port, &addr);
		int rc = uv_tcp_bind(&req->handle, (const struct sockaddr*)&addr, 0);
		if (rc)
			printf("[on_work] uv_tcp_bind error %s\n", uv_strerror(rc));

		int r = uv_listen((uv_stream_t*)&req->handle, MAX_CONNECT, on_new_connection);
		if (r) {
			printf("[on_work] listen error %s\n", uv_strerror(r));
			return;
		}

		char saddr[260] = { 0 };
		sprintf_s(saddr, "%s:%d", wr->ip, wr->port);
		printf("[on_work] sever %s started \r\n", saddr);

		uv_run(loop, UV_RUN_DEFAULT);

		uv_loop_close(loop);

		uv_close((uv_handle_t*)&req->handle, NULL);
		free(req);

		printf("[on_work] sever %s closed \r\n", saddr);
	}

	void close_work(uv_work_t *work, int status)
	{
		req_uv_work* wr = (req_uv_work*)work;
		char saddr[260] = { 0 };				
		if (wr)
		{
			close_conns(wr);
			sprintf_s(saddr, "%s:%d", wr->ip, wr->port);
			if (wr->srv != NULL)
			{
				wa::CriticalSession& lk = wr->srv->getLock();				
				THREAD_LIB_LOCK_CRITICAL_SESSION(lk)
				std::map<int, req_uv_work*>& workers = wr->srv->getWorkers();
				if (workers.find(wr->port) != workers.end())
					workers.erase(wr->port);
			}			
			free(wr);
			printf("[close_work] worker %s stopped \r\n", saddr);
		}
	}

	void wa_tcp_server::getList(std::vector<int>& srvs)
	{
		srvs.clear();
		THREAD_LIB_LOCK_CRITICAL_SESSION(_lock)
		for (std::map<int, req_uv_work*>::iterator it = _workers.begin();
				it != _workers.end(); ++it)
		{
			req_uv_work* wk = it->second;
			if (wk)
			{
				srvs.push_back(wk->port);
			}
		}
	}

	void wa_tcp_server::stop(int port)
	{
		THREAD_LIB_LOCK_CRITICAL_SESSION(_lock)
		if (_workers.find(port) != _workers.end())
		{
			req_uv_work* wk = _workers[port];
			uv_async_send(&wk->async);
		}
	}

	void wa_tcp_server::start(int port)
	{
		req_uv_work* req = (req_uv_work*)malloc(sizeof(req_uv_work));
		memset(req, 0, sizeof(req_uv_work));

		strcpy_s(req->ip, "::/128");
		req->port = port;		
		req->srv = this;

		uv_loop_init(&req->loop);
		uv_queue_work(_loop, &req->work, on_work, close_work);
		{
			THREAD_LIB_LOCK_CRITICAL_SESSION(_lock)
			if (_workers.find(port) == _workers.end())
				_workers[port] = req;
		}
	}

	void wa_tcp_server::start(uv_loop_t* loop, wa_tcp_conf* conf, int nsrv)
	{		
		for (int i = 0; i < nsrv; i++, conf++)
		{								
			start(conf->port);
		}		
	}

	void wa_tcp_server::shutdown()
	{
		printf("[shutdown] on_server down...\n");

		{
			THREAD_LIB_LOCK_CRITICAL_SESSION(_lock)
			std::queue<req_uv_work*> tasks;
			for (std::map<int, req_uv_work*>::iterator it = _workers.begin();
					it != _workers.end(); ++it)
			{
				req_uv_work* wk = it->second;
				if (wk)
				{
					tasks.push(wk);					
				}
			}
			while (!tasks.empty())
			{
				req_uv_work* wk = tasks.front();
				tasks.pop();
				uv_async_send(&wk->async);
			}
		}
	}
}