#include "stdafx.h"
#include "test_net.h"

namespace test_net
{
	void free_handle(uv_handle_t* handle)
	{
		free(handle);
	}

	void alloc_buffer(uv_handle_t* handle, size_t suggested_size, uv_buf_t* buf)
	{
		*buf = uv_buf_init((char*)malloc(suggested_size), suggested_size);
	}

	void echo_write(uv_write_t *req, int status) 
	{
		if (status == -1) {
			printf("Write error!\n");
		}
		char *base = (char*)req->data;
		free(base);
		free(req);
	}

	void echo_read(uv_stream_t* client, ssize_t nread, const uv_buf_t* buf)
	{
		if (nread == -1) {
			printf("Read error!\n");
			uv_close((uv_handle_t*)client, free_handle);
			return;
		}

		uv_write_t *write_req = (uv_write_t*)malloc(sizeof(uv_write_t));
		write_req->data = (void*)buf->base;
		uv_write(write_req, client, buf, 1, echo_write);
	}

	void on_new_connection(uv_stream_t *server, int status)
	{
		if (status < 0) {
			printf("New connection error %s\n", uv_strerror(status));
			// error!
			return;
		}

		req_uv_stream* req = (req_uv_stream*)server;
		uv_tcp_t *client = (uv_tcp_t*)malloc(sizeof(uv_tcp_t));
		uv_tcp_init(req->loop, client);
		if (uv_accept((uv_stream_t*)&req->server, (uv_stream_t*)client) == 0) {
			uv_read_start((uv_stream_t*)client, alloc_buffer, echo_read);
		}
		else {
			uv_close((uv_handle_t*)client, free_handle);
		}
	}

	void on_work(uv_work_t *work)
	{
		req_uv_work* wr = (req_uv_work*)work;

		uv_loop_t* loop = (uv_loop_t*)malloc(sizeof(uv_loop_t));
		uv_loop_init(loop);	

		req_uv_stream* req = (req_uv_stream*)malloc(sizeof(req_uv_stream));
		memset(req, 0, sizeof(req_uv_stream));
		req->loop = loop;

		uv_tcp_init(loop, &req->server);

		struct sockaddr_in addr;
		uv_ip4_addr(wr->ip, wr->port, &addr);
		int rc = uv_tcp_bind(&req->server, (const struct sockaddr*)&addr, 0);
		if (rc)
			printf("uv_tcp_bindv6 error %s\n", uv_strerror(rc));

		int r = uv_listen((uv_stream_t*)&req->server, MAX_CONNECT, on_new_connection);
		if (r) {
			printf("Listen error %s\n", uv_strerror(r));
			return;
		}
		
		char saddr[260] = { 0 };
		sprintf_s(saddr, "%s:%d", wr->ip, wr->port);
		printf("sever %s started \r\n", saddr);

		uv_run(loop, UV_RUN_DEFAULT);
		uv_loop_close(loop);
		free(req);
		free(loop);
	}

	void close_work(uv_work_t *work, int status)
	{
		req_uv_work* wr = (req_uv_work*)work;
		char saddr[260] = { 0 };
		sprintf_s(saddr, "%s:%d", wr->ip, wr->port);
		printf("sever %s stopped \r\n", saddr);
	}


	void start_tcp_server(const char* ip, int port)
	{
		uv_loop_t* loop = uv_default_loop();

		req_uv_work* req = (req_uv_work*)malloc(sizeof(req_uv_work));
		memset(req, 0, sizeof(req_uv_work));
		
		strcpy_s(req->ip, ip);
		req->port = port;
		req->default_loop = loop;

		uv_queue_work(loop, &req->work, on_work, close_work);

		uv_run(loop, UV_RUN_DEFAULT);

		free(req);
	}
}