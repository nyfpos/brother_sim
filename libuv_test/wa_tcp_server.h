#pragma once
#include "lock.h"
#include <iostream>
#include <map>
#include "iwa_tcp_conn.h"

typedef wa::iwa_tcp_conn* (*make_wa_conn)(int);
typedef void(*del_wa_conn)(wa::iwa_tcp_conn*);

namespace wa
{
	class wa_tcp_server;

	struct req_uv_write
	{
		uv_write_t handle;
		uv_tcp_t* client;
		uv_buf_t* buf;	
		iwa_tcp_conn* iwa_conn;
	};

	/*
	struct req_uv_stream
	{
		uv_tcp_t  handle;
		uv_loop_t* loop;
		uv_async_t* async;
		iwa_tcp_conn* iwa_conn;
	};
	*/

	struct req_server
	{
		uv_tcp_t  handle;
		uv_loop_t* loop;
		uv_async_t* async;
	};

	struct req_uv_work;

	struct req_conn
	{
		uv_tcp_t  handle;
		uv_async_t* async;
		iwa_tcp_conn* iwa_conn;
		req_uv_work* work;
		req_conn* next;
	};

	struct req_uv_work
	{
		uv_work_t work;
		uv_loop_t loop;
		uv_async_t async;
		char ip[64];
		int port;
		wa_tcp_server* srv;
		req_conn* conns;
	};

	struct wa_tcp_conf
	{
		int port;
	};

	class wa_tcp_server
	{
	public:
		wa_tcp_server(uv_loop_t* loop);
		~wa_tcp_server();
		void start(uv_loop_t* loop, wa_tcp_conf* conf, int nsrv);
		void start(int port);
		void stop(int port);
		void getList(std::vector<int>& srvs);
		void shutdown();

		static make_wa_conn makeConn;
		static del_wa_conn delConn;

		wa::CriticalSession& getLock() { return _lock; }
		std::map<int, req_uv_work*>& getWorkers() { return _workers; }

	private:
		//std::vector<req_uv_work*> _workers;	
		std::map<int, req_uv_work*> _workers;
		wa::CriticalSession _lock;
		uv_loop_t* _loop;
	};

}