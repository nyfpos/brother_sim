#include "stdafx.h"
#include <iostream>
#include <string>
#include "webserver.h"
#include "http_api_controller.h"
#include <nlohmann/json.hpp>
#include "util.h"
using json = nlohmann::json;

namespace waweb
{
	void _dump(json& o, char** outary, int* outsize)
	{
		std::string s = o.dump();
		int sz = s.length();
		char* p = (char*)malloc(sz + 1);
		memset(p, 0, sz + 1);
		memcpy_s(p, sz, s.c_str(), sz);
		*outary = p;
		*outsize = sz;
	}

	http_api_controller::http_api_controller(iwa_webapi srvApi):_srvApi(srvApi)
	{
	}


	http_api_controller::~http_api_controller()
	{		
	}

	bool http_api_controller::_api_list(struct message& ctx, char** outary, int* outsize)
	{
		bool ok(false);		
		try
		{
			_srvs.clear();
			_srvApi.list(_srvs);

			json o;
			o["err"] = 0;
			o["reason"] = "";
			o["result"] = json::array();
			for (std::vector<int>::iterator it = _srvs.begin();
				it != _srvs.end();
				++it)
			{
				o["result"].push_back(*it);
			}
			_dump(o, outary, outsize);
			ok = true;
		}
		catch (std::exception ex) {
			printf("[web-api] failed to parse json.\n");
		}		
		return ok;
	}

	bool http_api_controller::_api_start(struct message& ctx, char** outary, int* outsize)
	{		
		bool ok(false);
		std::string req = ctx.request_url;
		auto ls = util::split(req, "/");
		if (ls.size() >= 3)
		{
			int port = atoi(ls[2].c_str());
			_srvApi.start(port);
			json o;
			o["err"] = 0;
			o["reason"] = "";
			o["result"] = json::object();			
			_dump(o, outary, outsize);
			ok = true;
		}
		return ok;
	}

	bool http_api_controller::_api_stop(struct message& ctx, char** outary, int* outsize)
	{
		bool ok(false);
		std::string req = ctx.request_url;
		auto ls = util::split(req, "/");
		if (ls.size() >= 3)
		{
			int port = atoi(ls[2].c_str());
			_srvApi.stop(port);
			json o;
			o["err"] = 0;
			o["reason"] = "";
			o["result"] = json::object();
			_dump(o, outary, outsize);
			ok = true;
		}
		return ok;
	}

	bool http_api_controller::get_api_resp(struct message& ctx, char** outary, int* outsize)
	{
		bool ok(true);
		int bary_sz = 0;
		char* bary = NULL;		
		try
		{				
			std::string req = ctx.request_url;
			if (req.find("list") != std::string::npos)
			{
				ok = _api_list(ctx, outary, outsize);
			} else if (req.find("start") != std::string::npos)
			{
				ok = _api_start(ctx, outary, outsize);
			} else if (req.find("stop") != std::string::npos)
			{
				ok = _api_stop(ctx, outary, outsize);
			} else
				ok = false;			
		} catch (std::exception ex) {
			printf("[web-api] failed to parse json.\n");
			ok = false;
		}
		if (!ok)
		{
			json o;
			o["err"] = -1;
			o["reason"] = "Unknown request!";
			o["result"] = json::object();
			_dump(o, outary, outsize);
		}
		return true;
	}
}