// libuv_test.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "test_fs.h"
#include "test_event.h"
#include "test_thread.h"
#include "test_net.h"
#include "wa_tcp_server.h"
#include "broConn.h"
#include "webserver.h"
#include "test_json.h"
#include "http_request_handler.h"

#define WA_SERVER

// api
static wa::wa_tcp_server* g_srv = NULL;
void _webapi_start(int port)
{
	if (NULL != g_srv)
	{
		g_srv->start(port);
	}
}
void _webapi_stop(int port)
{
	if (NULL != g_srv)
	{
		g_srv->stop(port);
	}
}
void _webapi_list(std::vector<int>& srvs)
{
	if (NULL != g_srv)
	{
		g_srv->getList(srvs);
	}
}
//

struct req_uv_timer
{
	wa::wa_tcp_server* srv;
	int port;
};

wa::iwa_tcp_conn* makeBroConn(int port)
{
	return (wa::iwa_tcp_conn*)(new wa::broConn(port));
}

void delBroConn(wa::iwa_tcp_conn*conn)
{
	if (conn)
	{
		wa::broConn* broConn = (wa::broConn*)conn;
		if (broConn)
			delete broConn;
	}
}

void timer_cb1(uv_timer_t* timer, int status) 
{		
	req_uv_timer* para = (req_uv_timer*)timer->data;
	wa::wa_tcp_server*srv = para->srv;
	srv->stop(para->port);
	uv_timer_stop(timer);
	if (para->port == 10003)
	{
		srv->start(10000);
	}
}

int main(int argc, char** argv)
{	
	_wputenv(L"UV_THREADPOOL_SIZE=64");
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stderr, NULL, _IONBF, 0);
	// test_fs::start();
	//test_event::start();
	//test_thread::start();
	//test_net_cli::start_tcp_client("127.0.0.1", 10000);
	//test_json::test1();
	//test_json::test2();
#ifdef WA_SERVER
	const int MAX_SERV_SZ = 50;
	int nsrv = 50;
	int port = 10000;
	int broDevType = 1;
	int webport = 5678;
	if (argc >= 3)
	{
		port = atoi(argv[1]);
		nsrv = atoi(argv[2]);
		if (argc >= 4)
		{
			wa::broConn::DeviceType = atoi(argv[3]);
		} else {
			wa::broConn::DeviceType = broDevType;
		}
		if (argc >= 5)
		{
			webport = atoi(argv[4]);
		}
	}

	if (nsrv > MAX_SERV_SZ)
	{
		nsrv = MAX_SERV_SZ;
	}
	uv_loop_t* loop = uv_default_loop();

	wa::wa_tcp_server srv(loop);
	wa::wa_tcp_server::makeConn = makeBroConn;
	wa::wa_tcp_server::delConn = delBroConn;

	wa::wa_tcp_conf* confs = (wa::wa_tcp_conf*)malloc(sizeof(wa::wa_tcp_conf)*nsrv);
	wa::wa_tcp_conf* p = confs;
	memset(confs, 0, sizeof(wa::wa_tcp_conf)*nsrv);	

	printf("brother device type:%d\n", wa::broConn::DeviceType);

	waweb::iwa_webapi api;
	g_srv = &srv;
	api.start = _webapi_start;
	api.stop = _webapi_stop;
	api.list = _webapi_list;
	waweb::request_handler = new waweb::http_request_handler(api);
	/*
	const int MAX_TIMER_SIZE = 256;
	uv_timer_t timer[MAX_TIMER_SIZE] = {0};
	req_uv_timer tr_param[MAX_TIMER_SIZE] = { 0 };	
	*/
	for (int i = 0; i < nsrv; i++, p++)
	{
		p->port = port + i;
		/*
		if (i < MAX_TIMER_SIZE)
		{
			tr_param[i].srv = &srv;
			tr_param[i].port = p->port;
			uv_timer_init(loop, &timer[i]);
			timer[i].data = &tr_param[i];
			uv_timer_start(&timer[i], (uv_timer_cb)&timer_cb1, i*5000+5000, 1);
		}*/
	}	
	
	srv.start(loop, confs, nsrv);	

	waweb::start(loop, webport);

	uv_run(loop, UV_RUN_DEFAULT);

	srv.shutdown();

	uv_loop_close(loop);
#else
#ifdef _DEBUG
	argc = 5;
	argv = new char*[argc];
	argv[1] = "127.0.0.1";
	argv[2] = "10000";
	argv[3] = "-f";
	argv[4] = "C:/Users/junyu.ke/Desktop/code/C++/libuv_test/libuv_test/Release/xxx.txt";
#endif

	if (argc >= 5 && _strnicmp(argv[3], "-f", 2) == 0)
	{
		char ip[64] = { 0 };
		char message[1024] = { 0 };
		int port = 0;
		strcpy_s(ip, argv[1]);		
		port = atoi(argv[2]);

		if (_access(argv[4], 0) == 0)
		{
			FILE* fp = NULL;
			if (0 == fopen_s(&fp, argv[4], "r+t"))
			{
				fseek(fp, 0L, SEEK_END);
				size_t sz = ftell(fp);
				fseek(fp, 0L, SEEK_SET);
				char* message = (char*)malloc(sz);
				fread(message, 1, sz, fp);
				//printf("%s\n", message);
				fclose(fp);
				test_net_cli::start_tcp_client(ip, port, message, sz+1);				
			}
		}				
	}
	else if (argc >= 4)
	{
		char ip[64] = { 0 };
		char message[1024] = { 0 };
		int port = 0;
		strcpy_s(ip, argv[1]);
		strcpy_s(message, argv[3]);
		port = atoi(argv[2]);
		printf("org %s:%d %s\r\n", ip, port, message);
		test_net_cli::start_tcp_client(ip, port, message, sizeof(message));
	}
#endif

	system("PAUSE");
	return 0;
}
